package mindslab.ai.ejnlogviewer.service;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mindslab.ai.ejnlogviewer.db.EjnDao;
import mindslab.ai.ejnlogviewer.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class ViewerService {

    private Logger logger = LoggerFactory.getLogger(ViewerService.class);
    private final int LIMITE_CONNECT_COUNT = 5;
    private final int CONNECTION_TIME_OUT = 5000;
    private final int READ_TIME_OUT = 1000;


    @Autowired
    private EjnDao dao;

    public LoggerResult getData(String type, String date, String page) {
        // ejn API Connection
        String stringData = ejnApi(type, date, page);

        if (stringData == null) {
            logger.error("EJN API Connect Error");
            return null;
        }

        logger.debug(stringData);

        // ejn Json String parse
        LoggerResult result = parseEjnStringData(stringData);

        if (result == null) {
            return null;
        }

        logger.info(String.format("Get EJN Log-Data result : %s, %s, %s, %d", type, date, page, result.getTotalRows()));

        return result;
    }

    // ejn Json String parse
    private LoggerResult parseEjnStringData(String stringData) {
        // build Gson (under_score to carmelCase)
        Gson gson = buildGson();

        // dataFormat Setting
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
        SimpleDateFormat newSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.KOREA);

        // parse Json to LoggerResult
        LoggerResult resultData = gson.fromJson(stringData, LoggerResult.class);
        logger.debug(resultData.toString());

        List<LoggerInfo> Infos = resultData.getData();

        if (Infos == null) {
            return null;
        }

        // dataFormat Parse / Streamer data init
        for (int index = 0; index < Infos.size(); index++) {
            LoggerInfo temp = Infos.get(index);
            try {
                // parse new dataFormat
                temp.setCreatedAt(newSdf.format(sdf.parse(temp.getCreatedAt())));

                // init StreamerInfo
                temp.setStreamerInfo(twitchIdCompareDB(temp.getReceivedStreamerId()));

            } catch (ParseException e) {
                logger.error(e.getMessage());
            }
        }
        logger.debug(resultData.toString());

        return resultData;
    }

    // build Gson (under_score to carmelCase)
    private Gson buildGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setFieldNamingPolicy((FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES));
        return gsonBuilder.create();
    }

    // ejn Api Connection
    private String ejnApi(String type, String date, String page) {
        String urlString = "https://twip.kr/api/get_myvoice_log?apiKey=5c16f9dfea3afa8e8b0e59f99b2d0bef7d8921db&tts_type=" + type;
        String result = null;

        if (!date.equals("null")) {
            urlString += "&date=" + date;
        }

        if (!page.equals("0")) {
            urlString += "&page=" + page;
        }

        try {
            result = ejnApiConnector(urlString);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return null;
        }

        return result;
    }

    private String ejnApiConnector(String urlString) throws IOException{
        int connectCnt = 0;
//        logger.info(connectCnt + "");
        HttpURLConnection con = null;
        while (connectCnt < LIMITE_CONNECT_COUNT) {
            try {
                URL url = new URL(urlString);
                con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("GET");
                con.setConnectTimeout(CONNECTION_TIME_OUT);
                con.setReadTimeout(READ_TIME_OUT);

                int responseCode = con.getResponseCode();

                logger.info(String.format("EJN API Response Code : %d", responseCode));

                BufferedReader bufferedReader;
                if (responseCode == 200) {
                    bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                } else {
                    bufferedReader = new BufferedReader(new InputStreamReader(con.getErrorStream()));
                }

                String inputLine;
                StringBuffer response = new StringBuffer();
                while ((inputLine = bufferedReader.readLine()) != null) {
                    response.append(inputLine);
                }

                bufferedReader.close();
                logger.debug(response.toString());

                return response.toString();
            } catch (SocketTimeoutException exception) {
                logger.warn(String.format("Ejn API Connection %d times Fail", ++connectCnt));
            } finally {
                if (con != null) {
                    con.disconnect();
                }
            }
        }
        logger.info("EJN API Connection Fail");
        throw new IOException("Ejn API Connection Fail");
    }

    private StreamerInfo twitchIdCompareDB(String stringStreamerId) {

        int streamerId = Integer.parseInt(stringStreamerId);
        StreamerInfo result;

        if ((result = dao.selectStreamerInfo(streamerId)) == null) {
            result = parseStreamerInfo(twitchIdApi(stringStreamerId), streamerId);
            dao.insertStreamerInfo(result);
        }
        logger.debug(result.toString());
        return result;
    }

    // twitch ID Api Connection
    private String twitchIdApi(String streamerId) {
        String urlString = "https://api.twitch.tv/kraken/users/" + streamerId;

        try {
            URL url = new URL(urlString);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept", "application/vnd.twitchtv.v5+json");
            con.setRequestProperty("Client-ID", "4tycsahglcxxp8kfureos1wzdok4fi");

            int responseCode = con.getResponseCode();

            logger.info(String.format("Twitch API Response Code : %d", responseCode));
            BufferedReader bufferedReader;

            if (responseCode == 200) {
                bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            } else {
                bufferedReader = new BufferedReader(new InputStreamReader(con.getErrorStream()));
            }

            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = bufferedReader.readLine()) != null) {
                response.append(inputLine);
            }

            bufferedReader.close();
            logger.debug(response.toString());

            return response.toString();
        } catch (IOException e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    // parse twitch Json String to StreamerInfo
    private StreamerInfo parseStreamerInfo (String infoString, int streamerId) {
        Gson gson = buildGson();
        StreamerInfo streamerInfo;
        Map<String, Object> infoMap = gson.fromJson(infoString, Map.class);
        logger.debug(infoMap.toString());

        if (infoMap.containsKey("error")) {
            String status = String.valueOf(infoMap.get("status"));
            streamerInfo = new StreamerInfo(streamerId, status);
            return streamerInfo;
        }

        int id = Integer.parseInt((String) infoMap.get("_id"));
        streamerInfo = new StreamerInfo(id, (String) infoMap.get("name"),  (String) infoMap.get("display_name"));

        return streamerInfo;
    }

    public void countFee() {
        Calendar calendar = Calendar.getInstance(Locale.KOREA);
        calendar.add(Calendar.DATE, -1);
        Date yesterday = calendar.getTime();

        countFee(yesterday, "all");
    }

    private int countFee(Date date, String voiceName) {
        List<Voices> voices;

        if (voiceName.equals("all")) {
             voices = dao.selectVoices();
        } else {
            voices = dao.selectVoice(voiceName);
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        int result = 0;

        logger.debug(date.toString());

        for (Voices voice : voices) {

            DailyAggregate dailyAggregate = new DailyAggregate(
                    voice.getVoiceName(),
                    sdf.format(date)
            );

            if (dao.selectFeeDataHistory(dailyAggregate) > 0) {
                logger.info("Already input Fee-data : " + dailyAggregate.getVoiceName() + ", " + dailyAggregate.getDate());
                continue;
            }

            result++;
            int count = parseEjnStringData(ejnApi(voice.getVoiceName(), sdf.format(date), "1")).getTotalRows();
            int voiceFee = count * voice.getFee();

            dailyAggregate.setFees(
                    count,
                    voiceFee * voice.getFeeMlab() / 100,
                    voiceFee * voice.getFeeStreamer() / 100,
                    voiceFee * voice.getFeeMcn() / 100,
                    voiceFee * voice.getFeeEjn() / 100
            );
            logger.debug(dailyAggregate.toString());
            dao.insertFeeData(dailyAggregate);
        }

        return result;
    }

    public List<FeeInfo> getFee(String voiceName) {
        List<FeeInfo> feeInfos = dao.selectFeeInfo(voiceName);
        return feeInfos;
    }

    public int retroactFee(String retroactDate) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        int result = 0;
        try {
            Date date = simpleDateFormat.parse(retroactDate);
            result = countFee(date, "all");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return result;
    }

    public int insertDonations(String date) {

        List<Voices> voices = dao.selectVoices();
        List<LoggerInfo> donations = new ArrayList<>();

        try {

            for (Voices voice : voices) {

                Map<String, String> sqlParamMap = getDateMap(date);
                sqlParamMap.put("ttsType", voice.getVoiceName());
                if (donationChecker(date, voice) > 0) {
                    logger.info("Already input Done-data : " + sqlParamMap.toString());
                    continue;
                }
                int count = parseEjnStringData(ejnApi(voice.getVoiceName(), date, "1")).getTotalRows();
                if (count == 0) {
                    logger.info("Empty Done-data : " + sqlParamMap.toString());
                    continue;
                }
                int pageCount = (int) Math.floor(count / 100) + 1;
                logger.info("Input Done-data : " + voice.getVoiceName() + " " + count + ", " + pageCount);

                for (int index = 1; index <= pageCount; index++) {
                    String stringEjnData = ejnApi(voice.getVoiceName(), date, String.valueOf(index));
                    LoggerResult ejnData = parseEjnStringData(stringEjnData);
                    donations.addAll(ejnData.getData());
                }
            }

        } catch (ParseException e) {
            logger.error("Date Parse Exception", e);
        }

        logger.info("totalRow : " + donations.size());
        if (donations.size() != 0) {
            logger.info("DB init : " + dao.insertDonation(donations));
        }

        return donations.size();
    }

    private Map<String, String> getDateMap(String startDate) throws ParseException {
        Map<String, String> resultMap = new HashMap<>();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date inputDate = sdf.parse(startDate);

        calendar.setTime(inputDate);
        calendar.add(Calendar.DATE, 1);
        String endDate = sdf.format(calendar.getTime());
        resultMap.put("startDate", startDate);
        resultMap.put("endDate", endDate);

        return resultMap;
    }

    public String auth(String id, String password, HttpServletRequest request) {
        User user = new User(id, password);
        String result = "Success";
        if (dao.selectUser(user) == 0) {
            result = "False";
        }
        HttpSession session = request.getSession();
        session.setAttribute("user", user);

        logger.info("Login User : " + user.toString() + " " + result);

        return result;
    }

    public boolean sessionCheck(HttpServletRequest request) {
        HttpSession session = request.getSession();
        Object user = session.getAttribute("user");
        logger.info(user.toString());
        return user != null;
    }

    public void signOut(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.invalidate();
    }

    public String getUserId(HttpSession session) {
        try {
            return ((User) session.getAttribute("user")).getUserId();
        } catch (Exception e) {
            return "Unknown USER";
        }
    }

    public List<Voices> getVoices() {
        return dao.selectVoices();
    }

    private int donationChecker(String date, Voices voice) throws ParseException {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date parseDate = simpleDateFormat.parse(date);

        String beforeDateQuery = simpleDateFormat.format(parseDate) + " 00:00";
        calendar.setTime(parseDate);
        calendar.add(Calendar.DATE, 1);
        parseDate = calendar.getTime();
        String afterDateQuery = simpleDateFormat.format(parseDate) + " 00:00";

        LoggerInfo loggerInfo = new LoggerInfo();
        loggerInfo.setTtsType(voice.getVoiceName());
        loggerInfo.setCreatedAt(beforeDateQuery);
        loggerInfo.setAckAt(afterDateQuery);
        return dao.selectDoneDataHistory(loggerInfo);
    }
}
