package mindslab.ai.ejnlogviewer.config;

import mindslab.ai.ejnlogviewer.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Component
public class LoginInterceptor implements HandlerInterceptor {

    private Logger logger = LoggerFactory.getLogger(LoginInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if(request.getRequestURI().equals("/restart")){
//            System.out.println("RESTART");
            return true;
        }
//        System.out.println(request.getRequestURI());

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");

        if(ObjectUtils.isEmpty(user)) {
            response.sendRedirect("/auth");
            return false;
        }

        session.setMaxInactiveInterval(30*60);
        return true;
    }
}
