package mindslab.ai.ejnlogviewer.model;

public class DailyAggregate {
    private int id;
    private String voiceName;
    private String date;
    private int count;
    private double feeMlab;
    private double feeStreamer;
    private double feeMcn;
    private double feeEjn;

    public DailyAggregate() {
    }

    public DailyAggregate(String voiceName, String date) {
        this.voiceName = voiceName;
        this.date = date;
    }

    public void setFees(int count, double feeMlab, double feeStreamer, double feeMcn, double feeEjn) {
        this.count = count;
        this.feeMlab = feeMlab;
        this.feeStreamer = feeStreamer;
        this.feeMcn = feeMcn;
        this.feeEjn = feeEjn;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVoiceName() {
        return voiceName;
    }

    public void setVoiceName(String voiceName) {
        this.voiceName = voiceName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getFeeMlab() {
        return feeMlab;
    }

    public void setFeeMlab(double feeMlab) {
        this.feeMlab = feeMlab;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public double getFeeStreamer() {
        return feeStreamer;
    }

    public void setFeeStreamer(double feeStreamer) {
        this.feeStreamer = feeStreamer;
    }

    public double getFeeMcn() {
        return feeMcn;
    }

    public void setFeeMcn(double feeMcn) {
        this.feeMcn = feeMcn;
    }

    public double getFeeEjn() {
        return feeEjn;
    }

    public void setFeeEjn(double feeEjn) {
        this.feeEjn = feeEjn;
    }

    @Override
    public String toString() {
        return "DailyAggregate{" +
                "id=" + id +
                ", voiceName='" + voiceName + '\'' +
                ", date=" + date +
                ", feeMlab=" + feeMlab +
                ", count=" + count +
                ", feeStreamer=" + feeStreamer +
                ", feeMcn=" + feeMcn +
                ", feeEjn=" + feeEjn +
                '}';
    }
}
