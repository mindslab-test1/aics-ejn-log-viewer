package mindslab.ai.ejnlogviewer.model;

import java.util.Date;

public class FeeInfo {
    private String voiceName;
    private int fee;
    private String date;

    public String getVoiceName() {
        return voiceName;
    }

    public void setVoiceName(String voiceName) {
        this.voiceName = voiceName;
    }

    public int getFee() {
        return fee;
    }

    public void setFee(int fee) {
        this.fee = fee;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "FeeInfo{" +
                "voiceName='" + voiceName + '\'' +
                ", fee=" + fee +
                ", date=" + date +
                '}';
    }
}
