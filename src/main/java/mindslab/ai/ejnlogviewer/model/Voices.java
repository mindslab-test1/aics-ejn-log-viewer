package mindslab.ai.ejnlogviewer.model;

public class Voices {
    private int id;
    private int streamerId;
    private String voiceName;
    private int fee;
    private double feeMlab;
    private double feeStreamer;
    private double feeMcn;
    private double feeEjn;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStreamerId() {
        return streamerId;
    }

    public void setStreamerId(int streamerId) {
        this.streamerId = streamerId;
    }

    public String getVoiceName() {
        return voiceName;
    }

    public void setVoiceName(String voiceName) {
        this.voiceName = voiceName;
    }

    public int getFee() {
        return fee;
    }

    public void setFee(int fee) {
        this.fee = fee;
    }

    public double getFeeMlab() {
        return feeMlab;
    }

    public void setFeeMlab(double feeMlab) {
        this.feeMlab = feeMlab;
    }

    public double getFeeStreamer() {
        return feeStreamer;
    }

    public void setFeeStreamer(double feeStreamer) {
        this.feeStreamer = feeStreamer;
    }

    public double getFeeMcn() {
        return feeMcn;
    }

    public void setFeeMcn(double feeMcn) {
        this.feeMcn = feeMcn;
    }

    public double getFeeEjn() {
        return feeEjn;
    }

    public void setFeeEjn(double feeEjn) {
        this.feeEjn = feeEjn;
    }

    @Override
    public String toString() {
        return "Voices{" +
                "id=" + id +
                ", streamerId=" + streamerId +
                ", voiceName='" + voiceName + '\'' +
                ", fee=" + fee +
                ", feeMlab=" + feeMlab +
                ", feeStreamer=" + feeStreamer +
                ", feeMcn=" + feeMcn +
                ", feeEjn=" + feeEjn +
                '}';
    }
}
