package mindslab.ai.ejnlogviewer.model;

public class StreamerInfo {

    private int id;
    private int streamerId;
    private String streamerName;
    private String displayName;

    public StreamerInfo(int streamerId, String status) {
        this.streamerId = streamerId;
        this.streamerName = "Unknown User";
        this.displayName = "알 수 없는 사용자 ("+ status +")";
    }

    public StreamerInfo(int streamerId, String streamerName, String displayName) {
        this.streamerId = streamerId;
        this.streamerName = streamerName;
        this.displayName = displayName;
    }

    public StreamerInfo(int id, int streamerId, String streamerName, String displayName) {
        this(streamerId, streamerName, displayName);
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStreamerId() {
        return streamerId;
    }

    public void setStreamerId(int streamerId) {
        this.streamerId = streamerId;
    }

    public String getStreamerName() {
        return streamerName;
    }

    public void setStreamerName(String streamerName) {
        this.streamerName = streamerName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public String toString() {
        return "StreamerInfo{" +
                "id=" + id +
                ", streamerId=" + streamerId +
                ", streamerName='" + streamerName + '\'' +
                ", displayName='" + displayName + '\'' +
                '}';
    }
}
