package mindslab.ai.ejnlogviewer.model;

public class LoggerInfo {

    private String receivedStreamerId;
    private String userIdEncrypted;
    private String ttsType;
    private int donateAmount;
    private String donateComment;
    private String createdAt;
    private String ackAt;
    private StreamerInfo streamerInfo;

    public String getReceivedStreamerId() {
        return receivedStreamerId;
    }

    public void setReceivedStreamerId(String receivedStreamerId) {
        this.receivedStreamerId = receivedStreamerId;
    }

    public String getUserIdEncrypted() {
        return userIdEncrypted;
    }

    public void setUserIdEncrypted(String userIdEncrypted) {
        this.userIdEncrypted = userIdEncrypted;
    }

    public String getTtsType() {
        return ttsType;
    }

    public void setTtsType(String ttsType) {
        this.ttsType = ttsType;
    }

    public int getDonateAmount() {
        return donateAmount;
    }

    public void setDonateAmount(int donateAmount) {
        this.donateAmount = donateAmount;
    }

    public String getDonateComment() {
        return donateComment;
    }

    public void setDonateComment(String donateComment) {
        this.donateComment = donateComment;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getAckAt() {
        return ackAt;
    }

    public void setAckAt(String ackAt) {
        this.ackAt = ackAt;
    }

    public StreamerInfo getStreamerInfo() {
        return streamerInfo;
    }

    public void setStreamerInfo(StreamerInfo streamerInfo) {
        this.streamerInfo = streamerInfo;
    }

    @Override
    public String toString() {
        return "LoggerInfo{" +
                "receivedStreamerId='" + receivedStreamerId + '\'' +
                ", userIdEncrypted='" + userIdEncrypted + '\'' +
                ", ttsType='" + ttsType + '\'' +
                ", donateAmount=" + donateAmount +
                ", donateComment='" + donateComment + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", ackAt='" + ackAt + '\'' +
                ", streamerInfo=" + streamerInfo +
                '}';
    }
}
