package mindslab.ai.ejnlogviewer.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class LoggerResult {

    private String success;
    private int totalRows;
    private List<LoggerInfo> data;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public int getTotalRows() {
        return totalRows;
    }

    public void setTotalRows(int totalRows) {
        this.totalRows = totalRows;
    }

    public List<LoggerInfo> getData() {
        return data;
    }

    public void setData(List<LoggerInfo> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "LoggerResult{" +
                "success='" + success + '\'' +
                ", totalRows=" + totalRows +
                ", data=" + data +
                '}';
    }
}
