package mindslab.ai.ejnlogviewer.db;

import mindslab.ai.ejnlogviewer.model.*;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class EjnDao {
    protected static final String NAMESPACE = "mindslab.ai.ejnlogviewer.db.";

    @Autowired
    private SqlSession sqlSession;

    public StreamerInfo selectStreamerInfo(int streamerId) {
        return sqlSession.selectOne(NAMESPACE + "selectStreamerInfo", streamerId);
    }

    public int insertStreamerInfo(StreamerInfo streamerInfo) {
        return sqlSession.insert(NAMESPACE + "insertStreamerInfo", streamerInfo);
    }

    public List<Voices> selectVoices() {
        return sqlSession.selectList(NAMESPACE + "selectVoices");
    }

    public int insertFeeData(DailyAggregate feeData) {
        return sqlSession.insert(NAMESPACE + "insertFeeData", feeData);
    }

    public List<FeeInfo> selectFeeInfo(String voiceName) {
        return sqlSession.selectList(NAMESPACE + "selectFeeInfo", voiceName);
    }

    public int selectFeeDataHistory(DailyAggregate dailyAggregate) {
        return sqlSession.selectOne(NAMESPACE + "selectFeeDataHistory", dailyAggregate);
    }

    public int insertDonation(List<LoggerInfo> donations) {
        return sqlSession.insert(NAMESPACE + "insertDonation", donations);
    }

    public int selectDoneDataHistory(LoggerInfo loggerInfo) {
        return sqlSession.selectOne(NAMESPACE + "selectDoneDataHistory", loggerInfo);
    }

    public int selectUser(User user) {
        return sqlSession.selectOne(NAMESPACE + "selectUser", user);
    }

    public List<Voices> selectVoice(String voiceName) {
        return sqlSession.selectList(NAMESPACE + "selectVoice",  voiceName);
    }

}
