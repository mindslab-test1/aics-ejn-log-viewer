package mindslab.ai.ejnlogviewer.controller;

import mindslab.ai.ejnlogviewer.EjnLogViewerApplication;
import mindslab.ai.ejnlogviewer.model.Voices;
import mindslab.ai.ejnlogviewer.service.ViewerService;
import mindslab.ai.ejnlogviewer.model.FeeInfo;
import mindslab.ai.ejnlogviewer.model.LoggerResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

@Controller
public class ViewerController {

    private Logger logger = LoggerFactory.getLogger(ViewerController.class);

    @Autowired
    ViewerService service;

    @RequestMapping("/")
    public String index() { return "feeViewer.html"; }

    @RequestMapping("/auth")
    public String auth() { return "login.html"; }

    @RequestMapping("/logViewer")
    public String fee() { return "logViewer.html"; }

    @RequestMapping("/retroactive")
    public String retroactive() { return "retroactive.html"; }

    @RequestMapping("/newFee")
    public String newFee() { return "feeViewer.html"; }

    @RequestMapping("/signOut")
    public String signOut(HttpServletRequest request) {
        service.signOut(request);
        return auth();
    }

    @RequestMapping("/login")
    @ResponseBody
    public String login(
            @RequestParam("id") String id,
            @RequestParam("password") String password,
            HttpServletRequest request
            ) {
        return service.auth(id, password, request);
    }

    @RequestMapping("/getData")
    @ResponseBody
    public LoggerResult getData(
            @RequestParam(value="type") String type,
            @RequestParam(value="date", defaultValue = "null") String date,
            @RequestParam(value = "page", defaultValue = "0") String page,
            HttpSession session
    ) {
        logger.info(String.format("[%s] Get EJN Log-Data : %s, %s, %s", service.getUserId(session), type, date, page));
        return service.getData(type, date, page);
    }

    @RequestMapping("/getFee")
    @ResponseBody
    public List<FeeInfo> getFee(
            @RequestParam(value="voiceName", defaultValue = "everyOne") String voiceName,
            HttpSession session
    ) {
        logger.info(String.format("[%s] Get Fee-Data : %s", service.getUserId(session), voiceName));
        return service.getFee(voiceName);
    }

    @RequestMapping("/retroactFee")
    @ResponseBody
    public int retroFee(@RequestParam("date") String date, HttpSession session) {
        logger.info(String.format("[%s] Retroactive Fee : %s", service.getUserId(session), date));
        return service.retroactFee(date);
    }

    @RequestMapping("/retroactDone")
    @ResponseBody
    public int insertDonations(@RequestParam("date") String date, HttpSession session) {
        logger.info(String.format("[%s] Retroactive Donation : %s", service.getUserId(session), date));
        return service.insertDonations(date);
    }

    @RequestMapping("/getVoices")
    @ResponseBody
    public List<Voices> getVoices() {
        return service.getVoices();
    }

    @RequestMapping("/sessionCheck")
    @ResponseBody
    public boolean sessionCheck(HttpServletRequest request) {
        logger.info("SessionCheck!");
        return service.sessionCheck(request);
    }

    @RequestMapping("/restart")
    public void restart() {
        EjnLogViewerApplication.restart();
    }


    @Scheduled(cron = "0 0 03 * * ?")
    public void insertDonations() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        service.insertDonations(sdf.format(calendar.getTime()));
    }

    @Scheduled(cron = "0 30 03 * * ?")
    public void countFee() {
        service.countFee();
    }

    @Scheduled(cron = "0 0/1 * * * ?")
    public void reConnectDB(){
        System.out.print(service.getFee("everyOne").size());
        System.out.print(" ");
    }

}
